package com.example.alex.listviewadditem;

import java.io.Serializable;

public class User implements Serializable {

    private String name;//
    private String email;
    private String phone;
    private String adress;
    private int icon;

    public User(String name, String phone, int icon, String email, String adress) {
        this.name = name;
        this.phone = phone;
        this.icon = icon;
        this.email = email;
        this.adress = adress;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getIcon() {
        return icon;
    }

    public String getEmail() {
        return email;
    }

    public String getAdress() {
        return adress;
    }

    public User(){

    }
}

