package com.example.alex.listviewadditem;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutMeActivity  extends AppCompatActivity {

    @BindView(R.id.icon_aboutme)
    ImageView userImage;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.user_email)
    TextView userEmail;
    @BindView(R.id.adress)
    TextView userAdress;
    @BindView(R.id.phone)
    TextView userPhone;

    User userItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutme_activity);
        ButterKnife.bind(AboutMeActivity.this);
        Bundle b = getIntent().getExtras();
        if (b != null){
            userItem =(User) b.getSerializable("itemChoise");
        }
        //Toast.makeText(this, userItem.getName(), Toast.LENGTH_SHORT).show();
        userImage.setImageResource(userItem.getIcon());
        userName.setText(userItem.getName().toString());
        userEmail.setText(userItem.getEmail());
        userAdress.setText(userItem.getAdress());
        userPhone.setText(userItem.getPhone());
    }
}
