package com.example.alex.listviewadditem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    List<User> listUser = new ArrayList<>();//
    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(MainActivity.this);

        ListView listView = findViewById(R.id.list_view);
        myAdapter = new MyAdapter(this, generateUsers());
        listView.setAdapter(myAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User userItem = (User) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, AboutMeActivity.class);
                intent.putExtra("itemChoise", userItem);
                startActivity(intent);
                //Toast.makeText(MainActivity.this, userItem.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public List<User> generateUsers(){
        for(int i = 0; i<20; i++){
            listUser.add(new User("Alex" + i,
                    "+3809366302" + (i % 10),
                    getImageUser(),
                    "alex" + i + "@gmail.com",
                    "Kharkiv, Sumskaya str., " + (i % 10)));
        }
        return listUser;
    }

    @OnClick(R.id.button_add_item)
    public void onClick(){
        listUser.add(0, new User("User create", "+380936630299", getImageUser(), "userCreate123@gmail.com", "Kharkiv, Sumskaya str., 32"));
        myAdapter.notifyDataSetChanged();
    }

    public int getImageUser(){
        Random generator = new Random();
        int[] defoltIcon = new int[] {R.mipmap.user_icon_null_1, R.mipmap.user_icon_null_2,
                R.mipmap.user_icon_null_3, R.mipmap.user_icon_null_4, R.mipmap.user_icon_null_5};
        int imageId = defoltIcon[generator.nextInt(defoltIcon.length)];
        return imageId;
    }

}

