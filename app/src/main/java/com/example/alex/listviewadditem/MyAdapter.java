package com.example.alex.listviewadditem;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private List<User> users = new ArrayList<>();//

    public MyAdapter(Context context, @NonNull List<User> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        if(rowView == null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_view, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.userIcon = rowView.findViewById(R.id.icon);
            viewHolder.userName = rowView.findViewById(R.id.user_name);
            viewHolder.userMail = rowView.findViewById(R.id.user_email);

            rowView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) rowView.getTag();
        }

        viewHolder.userIcon.setImageResource(getItem(position).getIcon());
        viewHolder.userName.setText(getItem(position).getName());
        viewHolder.userMail.setText(getItem(position).getEmail());
        return rowView;
    }

    static class ViewHolder{
        ImageView userIcon;
        TextView userName;
        TextView userMail;
    }
}
